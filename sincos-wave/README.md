
# wave_practice

波動関数シミュレータ


![wave_practice2022.png](./wave_practice2022.png)


### 考慮事項



![wave_practice2022d.png](./wave_practice2022d.png)

上図の波形に於いてなめらかでない領域があり、直接sine関数を出力・合成して、プロットしている為である。

加速度から単位を組み立てる場合には、媒質相互の伝播と、それに依る、媒質毎のエネルギーの時間変化を計算する必要がある。




### references

- 物理学
  - 金原 寿郎, 大学演習 一般物理学, 裳華房, 1961初版[第7章 波動].
- OpenGL
  - [cl-glutによるマンデルブロ集合の描画 · GitHub](https://gist.github.com/soma-arc/f75dd1bcb79adf01c8ed)
  - [cl-openglでグラフィックス - Qiita](https://qiita.com/kedama17/items/6405790323b6e2917391)
