


(ql:quickload :asdf)
(ql:quickload :cl-opengl)
(ql:quickload :cl-glut)
(ql:quickload :cl-glu)


;; dimensions
(defun point (x y)
  (cons x y))

(defun point-x (p) (car p))
(defun point-y (p) (cdr p))

(defun distance (p1 p2)
  (sqrt (+ (expt (- (point-x p1) (point-x p2)) 2)
           (expt (- (point-y p1) (point-y p2)) 2))))

;;; field


(defun generate-field (&key (height 16) (width 16) (gap 0.1))
  ;; field of v=(x^2-y2, x^2+y-2, 0)
  (let* ((cent-y (/ (* gap height) 2))
         (cent-x (/ (* gap width) 2))
         (array (make-array (list height width) :initial-element (list 0 0 0))))
    (loop
      for i from 0 to (1- height)
      do (loop
           for j from 0 to (1- width)
           do (setf (aref array i j)
                    (list (- (expt (- cent-x (* gap j)) 2) (expt (- cent-y (* gap i)) 2))
                          (+ (expt (- cent-x (* gap j)) 2) (expt (- cent-y (* gap i)) 2))
                          0))))
    array))


;;;; open-GL

;;(asdf:oos 'asdf:load-op :cl-glut-examples)
;;(cl-glut-examples:rb-hello)


;; Parameters
(defvar *width* 500)
(defvar *height* 500)


;; Derived window class
(defclass main-window (glut:window) ()
    (:default-initargs :title "opengl test"
     :mode '(:double :rgb :depth)
     :width *width* :height *height*))


;; draw wave

(defun g2line (x y x2 y2)
  (gl:with-primitives :lines
    (gl:vertex x y 0.0)
    (gl:vertex x2 y2 0.0)))

(defun rect (x y &key (delta-x 1) (delta-y 1))
  (gl:with-primitives :polygon      
    (gl:vertex (- x delta-x) (- y delta-y) 0)
    (gl:vertex (- x delta-x) (+ y delta-y) 0)
    (gl:vertex (+ x delta-x) (+ y delta-y) 0)
    (gl:vertex (+ x delta-x) (- y delta-y) 0)
    ))


;; draw field

(defun draw-arrow (start delta gap)
  ;; (x, y, z=0) , (dx, dy, dz=0)
  (let ((sx (+ 100 (nth 0 start)))
        (sy (+ 100 (nth 1 start)))
        (dx (* gap (nth 0 delta)))
        (dy (* gap (nth 1 delta))))
    (gl:color 1.00 1.00 1.00)
    (rect sx sy :delta-x 0.5 :delta-y 0.5)
    (gl:with-primitives :lines
      (gl:vertex sx sy)
      (gl:vertex (- sx dx) (- sy dy)))
    (gl:color 1.0 1.0 1.0)
    ))

(defun draw ()
  (let* ((gap 20)
         (field (generate-field :height 20 :width 20 :gap (+ 0.1 (/ 1 gap))))
         (dim (array-dimensions field))
         )
    (loop
      for i from 0 to (1- (car dim))
      do (loop 
           for j from 0 to (1- (cadr dim))
           do (draw-arrow
               (list (* i 10) (* j 10) 0)
               (aref field i j)
               gap)))))



;;;
(defvar *the-time* 0)

(defun user-init ()
  (setf *the-time* 0)

  )

(defun user-idle ()
  (sleep (/ 1.0 30))
  (incf *the-time* 1))




(defun user-display ()
  (draw))

;; glut:display
;; Draw.
(defmethod glut:display ((window main-window))
  ;; Clear buffer
  (gl:clear :color-buffer :depth-buffer)
  
  ;; Draw shape
  ;;(glDisable GL_CULL_FACE)  
  (gl:shade-model :flat)
  (gl:normal 0 0 1.0)
  
  ;; user display process
  (user-display)
  
  ;; Swap buffer
  (glut:swap-buffers))

;; glut:idle
;; Application idle.
(defmethod glut:idle ((window main-window))
  ;; user idling process
  (user-idle)
  (glut:post-redisplay))

;; glut:reshape
(defmethod glut:reshape ((w main-window) width height)
  (gl:viewport 0 0 width height)
  (gl:load-identity)
  (glu:ortho-2d 0.0 *width* *height* 0.0))

;; glut-display
;; Draw.
(defmethod glut:display-window :before ((window main-window)) )

;; main
;; Main function (Program entry point).
(defun main ()
  ;; user initalization process
  (user-init)
  ;;(glut:special-func #'key-special)
  (glut:display-window (make-instance 'main-window)))


