




(in-package :cl-user)
(defpackage wave-equation-asd
  (:use :cl :asdf))
(in-package :wave-equation-asd)


;;
(defsystem wave-equation
  :name "wave-equation"
  :author "makinori"
  :version "1"
  :maintainer ""
  :licence "MIT"
  :description "simulator of wave-equation"
  :long-description "simulator of wave equation"
  :depends-on (#:cl-opengl #:cl-glut #:cl-glu)
  :components
  (;; package
   (:file "package")
   (:file "main")))
