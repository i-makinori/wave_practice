(in-package :wave-equation)

;; (ql:quickload :cl-opengl)


;; dimensions
(defun point (x y)
  (cons x y))

(defun point-x (p) (car p))
(defun point-y (p) (cdr p))

(defun distance (p1 p2)
  (sqrt (+ (expt (- (point-x p1) (point-x p2)) 2)
           (expt (- (point-y p1) (point-y p2)) 2))))

;;; wave functioons

(defun generate-wave-function (source amlitude omega velocity theta_0)
  #'(lambda (point time)
      (let* ((dist (distance point source))
             (delay (/ dist velocity)))
        (cond 
          ((<= time delay) 0)
          (t (* amlitude
                ;; (/ amlitude 2 pi dist)
                (sin (+ (* (- time delay) omega) theta_0))))))))

(defun generate-wave-function-sum-list (wave-list)
  #'(lambda (point time)
      (reduce #'+
              (mapcar #'(lambda (wave) (funcall wave point time)) wave-list))))



;;
(defparameter *amlitude* 1)
(defparameter *omega* (/ 12 pi))
(defparameter *velocity* 0.500)
(defparameter *theta_0* 0)

;;
(defparameter *num-mass-point* 400)
(defvar *the-time* 0)


;; sample wave



(defun wave-field-d (wave time &key (x-len 100) (y-len 100)
                                 (x-width  (* 4 pi)) (x-cent 0)
                                 (y-height (* 4 pi)) (y-cent 0))
  (let* ((field-array
           (make-array (list y-len x-len))) ;; :element-type 'double-float))
         (dx (/ x-width x-len))
         (dy (/ y-height y-len))
         (x-adj (+ (* 1/2 dx (1- x-len)) (- x-cent)))
         (y-adj (+ (* 1/2 dy (1- y-len)) (- y-cent)))
         (basis-convert
           #'(lambda (i j)
               (point (- (* i dx) x-adj)
                      (- (* j dy) y-adj)))))
    (loop for j from 0 to (1- y-len)
          do (loop for i from 0 to (1- x-len)
                   do (let ((val (funcall wave
                                          (funcall basis-convert i j)
                                          (* time 1/10))))
                        (setf (aref field-array j i) val))))
    field-array))




(defun wave-field (source time &key (x-len 100) (y-len 100)
                                      (x-width  (* 4 pi)) (x-cent 0)
                                      (y-height (* 4 pi)) (y-cent 0))
  (let* ((wave
           (generate-wave-function source *amlitude* *omega* *velocity* *theta_0*)))
    (wave-field-d wave time
                  :x-len x-len :y-len y-len 
                  :x-width x-width :x-cent x-cent
                  :y-height y-height :y-cent y-cent)))


(defun wave-array (source time)
  (let* ((line-len 400)
         (wave-field-matrix (wave-field source time :x-len line-len :y-len 1 ))
         (wave-line-array (make-array line-len)))
    (loop for i from 0 to (1- line-len)
          do (setf (aref wave-line-array i)
                   (aref wave-field-matrix 0 i)))
    wave-line-array))


(defun inter-ference-field (source-list time)
  (let* ((wave-list 
           (mapcar #'(lambda (source)
                       (generate-wave-function source *amlitude* *omega* *velocity* *theta_0*))
                   source-list))
         (wave (generate-wave-function-sum-list wave-list)))
    (wave-field-d wave time
                  :x-len 100 :y-len 100
                  :x-width (* pi 6) :x-cent 0
                  :y-height (* pi 6) :y-cent 0)))


;;;; open-GL

;; Parameters
(defvar *width* 500)
(defvar *height* 500)


;; Derived window class
(defclass main-window (glut:window) ()
    (:default-initargs :title "opengl test"
     :mode '(:double :rgb :depth)
     :width *width* :height *height*))


;; draw wave

(defun g2line (x y x2 y2)
  (gl:with-primitives :lines
    (gl:vertex x y 0.0)
    (gl:vertex x2 y2 0.0)))


(defun draw-wave-array (w-array)
  (let* ((pad 10)
         (x-len (array-dimension w-array 0))
         ;; (dx (/ (- *width* pad pad) x-len))
         (dx (/ (- 200 0) x-len))
         (y-cent (/ *height* 2)))
    (loop
      for i from 0 to (- x-len 1 1)
      do (g2line (+ pad (* dx (+ i 0)))
                 (+ y-cent (* 50 (aref w-array (+ i 0))))
                 (+ pad (* dx (+ i 1)))
                 (+ y-cent (* 50 (aref w-array (+ i 1))))))))


(defun color-rect (x y z &key (delta-x 1) (delta-y 1)
                              (color (gl:color 0 0 z)))
    (gl:with-primitives :polygon      
      color
      (gl:vertex (- x delta-x) (- y delta-y) 0)
      (gl:vertex (- x delta-x) (+ y delta-y) 0)
      (gl:vertex (+ x delta-x) (+ y delta-y) 0)
      (gl:vertex (+ x delta-x) (- y delta-y) 0)
      ))


(defun draw-wave-array-line (w-array)
  (let ((x-len (array-dimension w-array 0)))
    (loop for i from 0 to (- x-len 1)
          do (color-rect (+ 10 (* i 2)) 400 (aref w-array i)
                         :delta-x 1 :delta-y 10
                         :color (gl:color 0.2 0.2 (aref w-array i))))))


;; draw field

(defun draw-wave-field (w-field)
  (let ((y-len (array-dimension w-field 0))
        (x-len (array-dimension w-field 1)))
    (loop 
      for j from 0 to (1- y-len)
      do (loop for i from 0 to (1- x-len)
               do (color-rect (+ (* i 2) 10) (+ (* j 2) 10)
                              (aref w-field j i)
                              :delta-x 1 :delta-y 1
                              :color (gl:color 0 0.1 (/ (+ 0 (aref w-field j i)) 2 *amlitude*))
                              )))))


(defvar *displace-product-array*
  (make-array 100 :initial-element 0.00))

(defun draw ()
  (let ((y-equal 30) ;; width=100
        (inter-ference-field (inter-ference-field
                              (list (point -2 0) 
                                    (point 2 0)
                                    ;;(point 0 (* (sqrt 3) 2)))
                                    ;;(point (* 2 (cos (* 0.02 *the-time*)))
                                    ;;(* 2 (sin (* 0.02 *the-time*))))
                                    )
                              (* 1/1 *the-time*))))
    (draw-wave-field inter-ference-field)
    (gl:color 1 0 0)
    (g2line 10               (+ 10 (* y-equal 2))
            (+ 10 (* 100 2)) (+ 10 (* y-equal 2)))
    (let* ((field-x-len (array-dimension inter-ference-field 1))
           (displace-array (make-array field-x-len :initial-element 0.00))
           (product-array-len (array-dimension *displace-product-array* 0))
           (displace-product-average-array
             (make-array product-array-len)
             ))
      (loop for i from 0 to (1- field-x-len)
            do (setf (aref displace-array i)
                     (aref inter-ference-field y-equal i)))
      (loop for i from 0 to (1- product-array-len)
            do (setf (aref *displace-product-array* i)
                     (+ (aref *displace-product-array* i)
                     (abs (aref inter-ference-field y-equal i))))
               (if (zerop *the-time*) 0
                   (setf (aref displace-product-average-array i)
                         (* (/ *the-time*)
                            (aref *displace-product-array* i)
                        ))))
      (gl:color 1 1 1)
      (draw-wave-array displace-array)
      (draw-wave-array-line displace-product-average-array)
      )
    
    ))



;;;

(defun user-init ()
  (setf *the-time* 0)

  (setf *displace-product-array*
(make-array 100 :initial-element 0.00))
  )

(defun user-idle ()
  (sleep (/ 1.0 30))
  (incf *the-time* 1))




(defun user-display ()
  (draw))

;; glut:display
;; Draw.
(defmethod glut:display ((window main-window))
  ;; Clear buffer
  (gl:clear :color-buffer :depth-buffer)
  
  ;; Draw shape
  ;;(glDisable GL_CULL_FACE)  
  (gl:shade-model :flat)
  (gl:normal 0 0 1.0)
  
  ;; user display process
  (user-display)
  
  ;; Swap buffer
  (glut:swap-buffers))

;; glut:idle
;; Application idle.
(defmethod glut:idle ((window main-window))
  ;; user idling process
  (user-idle)
  (glut:post-redisplay))

;; glut:reshape
(defmethod glut:reshape ((w main-window) width height)
  (gl:viewport 0 0 width height)
  (gl:load-identity)
  (glu:ortho-2d 0.0 *width* *height* 0.0))

;; glut-display
;; Draw.
(defmethod glut:display-window :before ((window main-window)) )

;; main
;; Main function (Program entry point).
(defun main ()
  ;; user initalization process
  (user-init)
  ;;(glut:special-func #'key-special)
  (glut:display-window (make-instance 'main-window)))




