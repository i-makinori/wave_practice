(in-package :wave-equation)

;; (ql:quickload :cl-opengl)


;;; wave functioons



;;



;;; wave equation

(defparameter *velocity* 0.500)
(defvar *current-time* 0)
(defparameter *dt* 0.05)
(defparameter *dcell* 0.1)

(defparameter *width* 200)
(defparameter *height* 150)

(defun wave_t0 ()
  (let* ((field_t0 (make-array (list *width* *height*) :initial-element 0.0)))
    
    (loop for ix from 40 to 59
          do (loop for iy from 40 to 59
                   do (setf (aref field_t0 ix iy)
                            0.10)))
    
    (setf (aref field_t0 49 49) 1.0)
    field_t0))


;;; fixed end version
;; fixed end. u(0,t) = u(0,L) = 0 default

(defun aaref-fixed (array ix iy width height)
  
  
  ;;(declare (ignore width height))
  ;;(handler-case
  ;;(aref array ix iy)
  ;;(sb-int:invalid-array-index-error (e) e 0)))
  ;; free end. u default
  (cond ((or (< ix 0) (>= ix width)
             (< iy 0) (>= iy height))
         0)
        ;;
        (t (* 1 (aref array ix iy)))))


(defun updated-value-of-wave_t+dt-at-point-fixed (wave_t wave_t-dt x y)
  ;; not boundary
  ;;
  ;; 1D version,
  ;; u(x,t+dt) = 2*u(x,t) - u (x,t-dt) + (dt^2/dx^2)*{u(x+dx,t) + u(x-dx,t) - 2*u(x,t)}
  (+ (* 2  (aref wave_t   x y))
     (* -1 (aref wave_t-dt x y))
     ;;
     (* (expt *velocity* 2) (expt *dt* 2) (/ (expt *dcell* 2))
        (+ (* 1  (aaref-fixed wave_t (+ x 1) y *width* *height*))
           (* 1  (aaref-fixed wave_t (- x 1) y *width* *height*))
           (* 1  (aaref-fixed wave_t x (+ y 1) *width* *height*))
           (* 1  (aaref-fixed wave_t x (- y 1) *width* *height*))
           (* -4 (aref wave_t x y))))))

(defun update-wave_t+dt-fixed (wave_t wave_t-dt)
  ;;wave_t-1 time
  (let* ((wave_t+dt (make-array (array-dimensions wave_t) :initial-element 0.0))
         (len_x (array-dimension wave_t+dt 0))
         (len_y (array-dimension wave_t+dt 1)))
    (loop for ix from 0 to (1- len_x)
          do (loop for iy from 0 to (1- len_y)
                   do (setf (aref wave_t+dt ix iy)
                            (updated-value-of-wave_t+dt-at-point-fixed wave_t wave_t-dt ix iy))))
    (setf *current-time* (+ *current-time* *dt*))
    (values wave_t+dt wave_t)))


;;; ToDo : free end version
;; free end. (du/dx)(0,t) = (du/dx)(0,L) = 0 default

(defun aaref-free (array ix iy width height &optional (n-mirror 0)) ;;index of x and y
  ;; free end. u default
  (cond ((< ix 0)       (aaref-free array 0        iy       width height (+ n-mirror 1)))
        ((< iy 0)       (aaref-free array ix       0        width height (+ n-mirror 1)))
        ((>= ix width)  (aaref-free array (- ix 1) iy       width height (+ n-mirror 1)))
        ((>= iy height) (aaref-free array ix       (- iy 1) width height (+ n-mirror 1)))
        ;;
        ((= n-mirror 0) (aref array ix iy))
        (t (* 1 (aref array ix iy)))))

(defun updated-value-of-wave_t+dt-at-point-free (wave_t wave_t-dt x y)
  ;; not boundary
  ;;
  ;; 1D version,
  ;; u(x,t+dt) = 2*u(x,t) - u (x,t-dt) + (dt^2/dx^2)*{u(x+dx,t) + u(x-dx,t) - 2*u(x,t)}
  (+ (* 2  (aref wave_t   x y))
     (* -1 (aref wave_t-dt x y))
     ;;
     (* (expt *velocity* 2) (expt *dt* 2) (/ (expt *dcell* 2))
        (+ (* 1  (aaref-free wave_t (+ x 1) y *width* *height*))
           (* 1  (aaref-free wave_t (- x 1) y *width* *height*))
           (* 1  (aaref-free wave_t x (+ y 1) *width* *height*))
           (* 1  (aaref-free wave_t x (- y 1) *width* *height*))
           (* -4 (aref wave_t x y))))))

(defun update-wave_t+dt-free (wave_t wave_t-dt)
  ;;wave_t-1 time
  (let* ((wave_t+dt (make-array (array-dimensions wave_t) :initial-element 0.0))
         (len_x (array-dimension wave_t+dt 0))
         (len_y (array-dimension wave_t+dt 1)))
    ;; inner field
    (loop for ix from 0 to (1- len_x)
          do (loop for iy from 0 to (1- len_y)
                   do (setf (aref wave_t+dt ix iy)
                            (updated-value-of-wave_t+dt-at-point-free wave_t wave_t-dt ix iy))))
    
    ;; boundary free ends
    #|
    (loop for i from 0 to (1- len_x)
          do (setf (aref wave_t 0 0         ) 0.0)
             (setf (aref wave_t 0 (1- len_y)) 0.0))
    (loop for i from 0 to (1- len_y)
          do (setf (aref wave_t 0          i) 0.0)
             (setf (aref wave_t (1- len_x) i) 0.0))
    |#
    ;; time updation
    (setf *current-time* (+ *current-time* *dt*))

    (values wave_t+dt wave_t)))  



;; update wave and wave parameters


(defparameter *wave_t* #2A())
(defparameter *wave_t-1* #2A())

(defun init-wave-parameters ()
  (setf *current-time* 0)
  ;; initial conditions t=0
  (setf *wave_t* (wave_t0)) ;; initial shaped wave
  (setf *wave_t-1* (make-array (array-dimensions *wave_t*))) ;; stopped
  )

(defun update-wave-field! (wave_t wave_t-1 times)
  (let ((wave_t+n wave_t))
    (loop for i from 0 to (1- times)
          do (multiple-value-bind
                   (new-wave_t new-wave_t-1)
                 (update-wave_t+dt-fixed wave_t wave_t-1)
                 ;;(update-wave_t+dt-free wave_t wave_t-1)
               (setf wave_t+n new-wave_t)
               (setf wave_t-1 new-wave_t-1)))
    (values wave_t+n wave_t-1)))

(defun update-wave-and-status! (wave_t wave_t-1)
  (multiple-value-bind (new-wave_t+1 new-wave_t)
      (update-wave-field! wave_t wave_t-1 1)
    (setf *wave_t* new-wave_t+1)
    (setf *wave_t-1* new-wave_t)))



;;;; open-GL


;;; Draw wave field and etc


(defun color-rect (x y z &key (delta-x 1) (delta-y 1)
                              (color (gl:color 0 0 z)))
    (gl:with-primitives :polygon      
      color
      (gl:vertex (- x delta-x) (- y delta-y) 0)
      (gl:vertex (- x delta-x) (+ y delta-y) 0)
      (gl:vertex (+ x delta-x) (+ y delta-y) 0)
      (gl:vertex (+ x delta-x) (- y delta-y) 0)))


(defun draw-wave-field (wave_t wave_t-1)
  (let ((x-len (array-dimension wave_t 0))
        (y-len (array-dimension wave_t 1)))
    ;;(format t "~A, ~A~%" x-len y-len)
    (loop 
      for ix from 0 to (1- x-len)
      do (loop for iy from 0 to (1- y-len)
               do (color-rect
                   (+ (* ix 2) 10) (+ (* iy 2) 10)
                   ;;(aref wave_t ix iy)
                   1
                   :delta-x 1 :delta-y 1
                   ;;:color (gl:color (+ (* 0.4 (aref wave_t ix iy)))
                   ;;0.2
                   ;;(- (* 0.4 (aref wave_t ix iy)))))
                   :color (gl:color (+ 0.3 (* 0.3 (aref wave_t ix iy))) 
                                    (+ 0.5 (* 0.3 (aref wave_t-1 ix iy)))
                                    (- 0.3 (* 0.3 (aref wave_t ix iy)))))
               ))))


(defun render-string (x y string)
  (gl:color 1.0 1.0 1.0)
  (%gl:raster-pos-2f x y)
  (glut:bitmap-string #|glut:+bitmap-9-by-15+|# glut:+bitmap-helvetica-12+ string))


(defun draw-simulator-parameters! ()
  (render-string 20 400 (format nil "time: ~,4f~%" *current-time*))
  )


;;; Open GL

;; Parameters
(defvar *window-width* 500)
(defvar *window-height* 500)


;;; Derived window class
(defclass wave-equation-window (glut:window) ()
    (:default-initargs :title "opengl test"
     :mode '(:double :rgb :depth)
     :width *window-width* :height *window-height*))


(defun user-init ()
  (init-wave-parameters))

(defun user-idle ()
  (sleep (/ 1.0 30)))


(defun user-display ()
  ;; update
  (update-wave-and-status! *wave_t* *wave_t-1*)
  ;; draw
  (draw-wave-field *wave_t* *wave_t-1*)
  ;; draw parameters
  (draw-simulator-parameters!)
)

;; glut:display
;; Draw.
(defmethod glut:display ((window wave-equation-window))
  ;; Clear buffer
  (gl:clear :color-buffer :depth-buffer)
  
  ;; Draw shape
  (gl:shade-model :flat)
  (gl:normal 0 0 1.0)
  
  ;; user display process
  (user-display)
  
  ;; Swap buffer
  (glut:swap-buffers))


;; glut:idle
;; Application idle.
(defmethod glut:idle ((window wave-equation-window))
  ;; user idling process
  (user-idle)
  (glut:post-redisplay))

;; glut:reshape
(defmethod glut:reshape ((w wave-equation-window) width height)
  ;;(gl:viewport 0 0 width height)
  (gl:viewport 0 0 *window-width* *window-height*)
  (gl:load-identity)
  (glu:ortho-2d 0.0 *window-width* *window-height* 0.0))

;; glut:keyboard

(defun exit-window (window)
  window
  (glut:leave-main-loop)
  (glut:destroy-window (glut:get-window))
  (glut:destroy-current-window))


(defmethod glut:keyboard ((window wave-equation-window) key x y)
  (declare (ignore x y))
  ;;
  (cond ((eql key #\Esc) 
         (exit-window window))
        (t
         (glut:post-redisplay))))

;; glut-display
;; Draw.
(defmethod glut:display-window :before ((window wave-equation-window)) )

;; Main function (Program entry point).
(defun main ()
  ;; user initalization process
  (user-init)
  ;;(glut:special-func #'key-special)
  (glut:display-window (make-instance 'wave-equation-window)))




